import numpy as np
import pandas as pd
import cvxpy as cv

from scipy.linalg import sqrtm
from .param_estimates import mu_estimates, cov_matrix, black_litterman
from .util import is_positive_definite, cov_fix
from .risk_functions import mean_absolute_deviation, semi_deviation, conditional_value_at_risk, worst_realization, \
    lower_partial_moment


class Portfolio(object):
    """
    Framework for calculating constrained optimal portfolios.

    Parameters
    ----------
    returns : pd.DataFrame, shape [periods, m assets]
        Historical returns of asset candidates.

    allow_shorts : bool, default False
        If portfolio can consider short positions (negative weights).

    short_max : float, default 0.2
        Max value of sum of absolute values of short positions
        (negative weights).

    long_max : float, default 1
        Max value of the sum of long positions (positive weights).
        When allow_shorts is True, the difference between long_max and
        short_max must be equal to budget (long_max-short_max = budget).

    budget : float, default 1
        Max value of the sum of long positions (positive weights) and
        short positions (negative weights).

    min_number_effective_assets : int, default None
        Min number of effective assets used in portfolio.
        AKA inverse of Herfindahl-Hirschman index of weights.

    max_assets : int, default None
        Max number of assets used in portfolio. It requires a solver
        that supports Mixed Integer Programs (MIP).

    factor_returns : pd.DataFrame, shape [periods, p factors]
        Factor returns.

    fem : pd.DataFrame, shape [m securities, p factors]
        Factor exposures (betas) matrix.

    conditional_alpha : float, default 0.05
        Significance level of CVaR and CDaR.

    turnover_limit : float, default None
        Max allowed turnover.

    tracking_error_limit : float, default None
        Max tracking error limit.

    benchmark_detailed : bool, default True
        True if the benchmark is a portfolio with detailed weights and
        False if the benchmark is an index.

    benchmark_returns : pd.DataFrame
        If benchmark_detailed is False the tracking error constraints
        are calculated with respect to this benchmark.

    benchmark_weights : pd.DataFrame
        Default is the equally weighted portfolio 1/N.

    asset_inequality : nd-array, default None
        Asset level constraints. See .constraint_functions.

    group_inequality : 1d-array, default None
        Group level constraints (ie, sector). See .constraint_functions.

    min_return : float, default None
        Constraint on min level of expected return.

    """

    # solvers
    solvers = ["ECOS", "SCS", "OSQP", "CVXOPT"]
    solver_params = {
        # 'ECOS': {"max_iters": 500, "abstol": 1e-8},
        # 'SCS': {"max_iters": 2500, "eps": 1e-5},
        # 'OSQP': {"max_iter": 10000, "eps_abs": 1e-8},
        # 'CVXOPT': {"max_iters": 500, "abstol": 1e-8},
    }

    def __init__(
            self,
            returns=None,
            allow_shorts=False,
            short_max=0.2,
            long_max=1,
            budget=1,
            min_number_effective_assets=None,
            max_assets=None,
            factor_returns=None,
            fem=None,
            conditional_alpha=0.05,
            turnover_limit=None,
            tracking_error_limit=None,
            benchmark_detailed=True,
            benchmark_returns=None,
            benchmark_weights=None,
            asset_inequality=None,
            group_inequality=None,
            min_return=None,
            max_risk_measure=None,
    ):

        self.returns = returns
        self.allow_shorts = allow_shorts
        self.short_max = short_max
        self.long_max = long_max
        self.budget = budget
        self.min_number_effective_assets = min_number_effective_assets
        self.max_assets = max_assets
        self.factor_returns = factor_returns
        self.fem = fem
        self.conditional_alpha = conditional_alpha
        self.turnover_limit = turnover_limit
        self.tracking_error_limit = tracking_error_limit
        self.benchmark_detailed = benchmark_detailed
        self.benchmark_returns = benchmark_returns
        self.benchmark_weights = benchmark_weights
        self.asset_inequality = asset_inequality
        self.group_inequality = group_inequality
        self.min_return = min_return
        self.max_risk_measure = max_risk_measure

        # src inputs
        self.mu = None
        self.cov = None
        self.mu_f = None
        self.cov_f = None
        self.fem = None
        self.mu_fm = None
        self.cov_fm = None
        self.mu_bl = None
        self.cov_bl = None
        self.mu_bl_fm = None
        self.cov_bl_fm = None
        self.returns_fm = None
        self.nav_fm = None

    @property
    def returns(self):
        return self._returns

    @returns.setter
    def returns(self, value):
        if value is not None and not isinstance(value, pd.DataFrame):
            raise ValueError("returns must be pd.DataFrame")

        self._returns = value

    @property
    def nav(self):
        if self._returns is not None:
            return self._returns.cumsum()
        else:
            return None

    @property
    def assets(self):
        return list(self._returns.columns) if self._returns is not None else None

    @property
    def number_of_assets(self):
        return len(self.assets) if self.assets is not None else None

    @property
    def factor_returns(self):
        return self._factor_returns

    @factor_returns.setter
    def factor_returns(self, value):
        if value is not None and not self.returns.index.equals(value.index):
            raise ValueError(
                "factor_returns must be pd.DataFrame with matching index to returns."
            )

        self._factor_returns = value

    @property
    def factors(self):
        return (
            list(self._factor_returns.columns)
            if self._factor_returns is not None
            else None
        )

    @property
    def fem(self):
        return self._fem

    @fem.setter
    def fem(self, value):
        if value is not None and not isinstance(value, pd.DataFrame):
            raise ValueError("fem matrix must be a pd.DataFrame")

        self._fem = value

    @property
    def benchmark_weights(self):
        return self._benchmark_weights

    @benchmark_weights.setter
    def benchmark_weights(self, value):
        n = self.number_of_assets
        if value is not None:
            if not (value.shape[0] == n and value.shape[1] == 1):
                raise NameError("Weights must have a size of shape (n_assets,1)")
        else:
            value = np.array(np.ones((n, 1)) / n)

        self._benchmark_weights = value

    @property
    def asset_inequality(self):
        return self._asset_inequality

    @asset_inequality.setter
    def asset_inequality(self, value):
        if value is not None and value.shape[1] != self.number_of_assets:
            raise ValueError("Asset inequality shape must match number of assets")

        self._asset_inequality = value

    @property
    def group_inequality(self):
        return self._group_inequality

    @group_inequality.setter
    def group_inequality(self, value):
        if value is not None and value.shape[1] != 1:
            raise ValueError("The group_inequality must have one column")

        self._group_inequality = value

    @staticmethod
    def _check_is_positive_definite(value):
        if not is_positive_definite(value, threshold=1e-8):
            try:
                cov = cov_fix(value, method="clipped", threshold=1e-5)
                if not is_positive_definite(cov, threshold=1e-8):
                    raise ValueError(
                        "You must convert self.cov to a positive (semi)definite matrix"
                    )
            except:
                raise ValueError(
                    "You must convert self.cov to a positive (semi)definite matrix"
                )

        return value

    def asset_stats(self, method_mu="hist", method_cov="hist", **kwargs):
        """
        Calculate the inputs that will be used by the src method when
        we select the input model='Classic'.

        See Also
        --------
        param_estimates.mu_estimates
        param_estimates.cov_matrix
        """

        self.mu = mu_estimates(self.returns, method=method_mu, **kwargs)

        cov = cov_matrix(self.returns, method=method_cov, **kwargs)
        self.cov = self._check_is_positive_definite(cov)

        return self

    def blacklitterman_stats(
            self,
            views,
            views_mu,
            rf=0,
            w=None,
            delta=None,
            equilibrium=True,
            method_mu="hist",
            method_cov="hist",
            **kwargs,
    ):
        """
        Calculate src inputs when model='BL'.

        See Also
        --------
        param_estimates.black_litterman

        """
        X = self.returns
        if w is None:
            w = np.array(self.benchmark_weights, ndmin=2)

        if delta is None:
            if self.mu is None:
                self.asset_stats(
                    method_mu=method_mu, method_cov=method_cov, **kwargs
                )  # need to test

            a = np.array(self.mu, ndmin=2) @ np.array(w, ndmin=2)
            delta = (a - rf) / (
                    np.array(w, ndmin=2).T
                    @ np.array(self.cov, ndmin=2)
                    @ np.array(w, ndmin=2)
            )
            delta = delta.item()

        mu, cov, w = black_litterman(
            X=X,
            w=w,
            views=views,
            views_mu=views_mu,
            delta=delta,
            rf=rf,
            equilibrium=equilibrium,
            method_mu=method_mu,
            method_cov=method_cov,
            **kwargs,
        )

        self.mu_bl = mu
        self.cov_bl = self._check_is_positive_definite(cov)

        return self

    def _model_inputs(self, model, historical=True):
        mu, sigma, returns, nav = (None, None, None, None)

        if model == "Classic":
            mu = np.array(self.mu, ndmin=2)
            sigma = np.array(self.cov, ndmin=2)
            returns = np.array(self.returns, ndmin=2)
            nav = np.array(self.nav, ndmin=2)
        elif model == "BL":
            mu = np.array(self.mu_bl, ndmin=2)
            if historical is False:
                sigma = np.array(self.cov_bl, ndmin=2)
            else:
                sigma = np.array(self.cov, ndmin=2)
            returns = np.array(self.returns, ndmin=2)
            nav = np.array(self.nav, ndmin=2)
        elif model == "FM":
            mu = np.array(self.mu_fm, ndmin=2)
            if historical is False:
                sigma = np.array(self.cov_fm, ndmin=2)
                returns = np.array(self.returns_fm, ndmin=2)
                nav = np.array(self.nav_fm, ndmin=2)
            else:
                sigma = np.array(self.cov, ndmin=2)
                returns = np.array(self.returns, ndmin=2)
                nav = np.array(self.nav, ndmin=2)
        elif model == "BL_FM":
            mu = np.array(self.mu_bl_fm, ndmin=2)
            if historical is False:
                sigma = np.array(self.cov_bl_fm, ndmin=2)
                returns = np.array(self.returns_fm, ndmin=2)
                nav = np.array(self.nav_fm, ndmin=2)
            else:
                sigma = np.array(self.cov, ndmin=2)
                returns = np.array(self.returns, ndmin=2)
                nav = np.array(self.nav, ndmin=2)
            # elif False:
            #    sigma = np.array(self.cov_fm, ndmin=2)
            #    returns = np.array(self.returns_fm, ndmin=2)
            #    nav = np.array(self.nav_fm, ndmin=2)

        return mu, sigma, returns, nav

    # noinspection PyTypeChecker
    def _asset_weight_and_number_constraints(self, obj, mu, w, k):
        n = cv.Variable(
            (mu.shape[1], 1), boolean=True
        )  # will be used for 'max_assets', if applicable

        constraints = []
        if obj == "Sharpe":
            constraints += [cv.sum(w) == self.budget * k, k * 1000 >= 0]
            n_ref = cv.Variable((mu.shape[1], 1))

            if self.allow_shorts is False:
                constraints += [w <= self.long_max * k, w * 1000 >= 0]

                if self.max_assets is not None:
                    constraints += [
                        cv.sum(n) <= self.max_assets,
                        n_ref <= k,
                        n_ref >= 0,
                        n_ref <= 100000 * n,
                        n_ref >= k - 100000 * (1 - n),
                        w <= self.long_max * n_ref,
                    ]
            else:
                constraints += [
                    cv.sum(cv.pos(w)) * 1000 <= self.long_max * k * 1000,
                    cv.sum(cv.neg(w)) * 1000 <= self.short_max * k * 1000,
                ]
                if self.max_assets is not None:
                    constraints += [
                        cv.sum(n) <= self.max_assets,
                        n_ref <= k,
                        n_ref >= 0,
                        n_ref <= 100000 * n,
                        n_ref >= k - 100000 * (1 - n),
                        w >= -self.short_max * n_ref,
                        w <= self.long_max * n_ref,
                    ]

        else:  # non-Sharpe obj
            constraints += [cv.sum(w) == self.budget]
            if self.allow_shorts is False:
                constraints += [w <= self.long_max, w * 1000 >= 0]
                if self.max_assets is not None:
                    constraints += [
                        cv.sum(n) <= self.max_assets,
                        w <= self.long_max * n,
                    ]

            else:
                constraints += [
                    cv.sum(cv.pos(w)) * 1000 <= self.long_max * 1000,
                    cv.sum(cv.neg(w)) * 1000 <= self.short_max * 1000,
                ]
                if self.max_assets is not None:
                    constraints += [
                        cv.sum(n) <= self.max_assets,
                        w >= -self.short_max * n,
                        w <= self.long_max * n,
                    ]

        return constraints

    def _turnover_constraint(self, obj, w, c, k):
        constraint = []
        if self.turnover_limit is not None:
            if obj == "Sharpe":
                turnover_fx = cv.abs(w - c @ k) * 1000
                constraint = [turnover_fx <= self.turnover_limit * k * 1000]
            else:
                turnover_fx = cv.abs(w - c) * 1000
                constraint = [turnover_fx <= self.turnover_limit * 1000]

        return constraint

    def _tracking_error_constraint(self, obj, returns, w, c, k, n):

        if self.benchmark_detailed:
            bench = returns @ c
        else:
            bench = np.array(self.benchmark_returns, ndmin=2)

        constraint = []
        if self.tracking_error_limit is not None:
            if obj == "Sharpe":
                te_fx = cv.norm(returns @ w - bench @ k, "fro") / cv.sqrt(n - 1)
                constraint = [te_fx * 1000 <= self.tracking_error_limit * k * 1000]
            else:
                te_fx = cv.norm(returns @ w - bench, "fro") / cv.sqrt(n - 1)
                constraint = [te_fx * 1000 <= self.tracking_error_limit * 1000]

        return constraint

    def _min_return_constraint(self, obj, ret, k):
        constraint = []
        if self.min_return is not None:
            if obj == "Sharpe":
                constraint = [ret >= self.min_return * k]
            else:
                constraint = [ret >= self.min_return]

        return constraint

    def _min_assets_contraint(self, obj, w, k):
        constraint = []
        if self.min_number_effective_assets is not None:
            if obj == "Sharpe":
                constraint = [
                    cv.norm(w, "fro") <= 1 / self.min_number_effective_assets ** 0.5 * k
                ]
            else:
                constraint = [
                    cv.norm(w, "fro") <= 1 / self.min_number_effective_assets ** 0.5
                ]

        return constraint

    def _linear_constraints(self, obj, w, k):
        constraint = []
        if self.asset_inequality is not None and self.group_inequality is not None:
            A = np.array(self.asset_inequality, ndmin=2) * 1000
            B = np.array(self.group_inequality, ndmin=2) * 1000

            if obj == "Sharpe":
                constraint = [A @ w - B @ k >= 0]
            else:
                constraint = [A @ w - B >= 0]

        return constraint

    def _max_risk_measure_constraint(self, obj, risk_obj, k, scale=1):
        constraint = []
        if self.max_risk_measure is not None:
            if obj == "Sharpe":
                constraint = [risk_obj <= self.max_risk_measure * k / scale]
            else:
                constraint = [risk_obj <= self.max_risk_measure / scale]

        return constraint

    # noinspection PyTypeChecker
    def optimization(
            self,
            model="Classic",
            risk_measure="MV",
            obj="Sharpe",
            kelly=False,
            rf=0,
            risk_aversion=2,
            historical=True,
            **kwargs,
    ):
        """
        Calculate optimal portfolio according to model & params selected.

        Parameters
        ----------
        model : str, default 'Classic'
            The model used for optimizing the portfolio.

            - 'Classic': uses estimates of expected return vector and covariance matrix based on historical data.
            - 'BL': '' based on Black Litterman model.
            - 'FM': '' based on risk model specified by user.
            - 'BLFM': '' based on Black Litterman applied to risk model specified by user.

        risk_measure : str, default 'MV'
            The risk measure used to optimize the portfolio.

            - 'MV': Standard Deviation.
            - 'MAD': Mean Absolute Deviation.
            - 'MSV': Semi Standard Deviation.
            - 'FLPM': First Lower Partial Moment (Omega Ratio).
            - 'SLPM': Second Lower Partial Moment (Sortino Ratio).
            - 'CVaR': Conditional Value at Risk.
            - 'WR': Worst Realization (Minimax)
            - 'MDD': Maximum Drawdown of uncompounded cumulative returns (Calmar Ratio).
            - 'ADD': Average Drawdown of uncompounded cumulative returns.
            - 'CDaR': Conditional Drawdown at Risk of uncompounded cumulative returns.
            - 'UCI': Ulcer Index of uncompounded cumulative returns.

        obj : str, default 'Sharpe'
            Objective function to optimize the portfolio.

            - 'MinRisk': Minimize selected risk measure.
            - 'Utility': Maximize Utility function.
            - 'Sharpe': Maximize risk adjusted return ratio based on selected risk measure.
            - 'MaxRet': Maximize expected return.

        kelly : str, default False
            Method used to calculate mean return.
            Only relevant for model == 'Classic'.

            - False : arithmetic return
            - "exact" :  logarithmic return.
            - "approx" : approximate logarithmic using first and second moment

        rf : float, default 0
            Risk free rate, must be in same period of assets returns.

        risk_aversion : scalar, default 2
            Risk aversion factor. Only relevant for obj == 'Utility'.

        historical : bool, default True
            Which kind of returns used to calculate risk measures.
            Ignored if model == 'Classic'.

        Returns
        -------
        w : DataFrame
            Corresponding weights of optimal portfolio.

        """

        if model == "Classic" or historical:
            self.asset_stats(method_mu="hist", method_cov="hist")

        mu, sigma, returns, nav = self._model_inputs(model, historical)

        # general variables
        w = cv.Variable((mu.shape[1], 1))
        k = cv.Variable((1, 1))
        rf0 = rf
        n = returns.shape[0]
        gr = cv.Variable((n, 1))

        X = returns @ w
        ret = mu @ w

        # override 'ret' based on Classic parameters
        g = cv.Variable(nonneg=True)  # aux
        if model == "Classic":
            if kelly == "exact":
                if obj == "Sharpe":
                    ret = 1 / n * cv.sum(gr) - rf0 * k
                else:
                    ret = 1 / n * cv.sum(cv.log(1 + returns @ w))
            elif kelly == "approx":
                if obj == "Sharpe":
                    ret = mu @ w - 0.5 * cv.quad_over_lin(g, k)
                else:
                    ret = mu @ w - 0.5 * g ** 2

        constraints = []
        risk_constraints = []
        risk = None
        if risk_measure == "MV":
            G = sqrtm(sigma)

            risk = g
            risk_constraints = [cv.SOC(g, G.T @ w)]
            constraints += self._max_risk_measure_constraint(obj, risk, k)

        elif risk_measure in ["MAD", "MSV"]:  # mad models
            Y = cv.Variable((returns.shape[0], 1))
            u = np.ones((returns.shape[0], 1)) * mu
            a = returns - u

            risk = None
            if risk_measure == "MAD":
                risk = cv.sum(Y) / n
            elif risk_measure == "MSV":
                risk = cv.norm(Y, "fro") / cv.sqrt(n - 1)

            risk_constraints = [a @ w >= -Y, Y >= 0]
            constraints += self._max_risk_measure_constraint(
                obj, risk, k, scale=2 if risk_measure == "MAD" else 1
            )

        elif risk_measure == "CVaR":
            VaR = cv.Variable((1, 1))
            Z = cv.Variable((returns.shape[0], 1))

            risk = VaR + 1 / (self.conditional_alpha * n) * cv.sum(Z)
            risk_constraints = [Z >= 0, Z >= -X - VaR]
            constraints += self._max_risk_measure_constraint(obj, risk, k)

        elif risk_measure == "WR":
            M = cv.Variable((1, 1))

            risk = M
            risk_constraints = [-X <= M]
            constraints += self._max_risk_measure_constraint(obj, risk_obj=-X, k=k)

        elif risk_measure in ["FLPM", "SLPM"]:
            lpm = cv.Variable((returns.shape[0], 1))
            risk_constraints = [lpm >= 0]

            if obj == "Sharpe":
                risk_constraints += [lpm >= rf0 * k - X]
            else:
                risk_constraints += [lpm >= rf0 - X]

            risk = None
            if risk_measure == "FLPM":
                risk = cv.sum(lpm) / n
            elif risk_measure == "SLPM":
                risk = cv.norm(lpm, "fro") / cv.sqrt(n - 1)

            constraints += self._max_risk_measure_constraint(obj, risk, k)

        elif risk_measure in ["MDD", "ADD", "CDaR", "UCI", "EDaR"]:
            if obj == "Sharpe":
                X1 = k + nav @ w
            else:
                X1 = 1 + nav @ w

            U = cv.Variable((nav.shape[0] + 1, 1))
            risk_constraints = [
                U[1:] * 1000 >= X1 * 1000,
                U[1:] * 1000 >= U[:-1] * 1000,
            ]

            if obj == "Sharpe":
                risk_constraints += [U[1:] * 1000 >= k * 1000, U[0] * 1000 == k * 1000]
            else:
                risk_constraints += [U[1:] * 1000 >= 1 * 1000, U[0] * 1000 == 1 * 1000]

            risk = None
            if risk_measure == "MDD":
                MDD = cv.Variable((1, 1))
                risk = MDD
                risk_constraints += [MDD >= U[1:] - X1]
                constraints += self._max_risk_measure_constraint(
                    obj, risk_obj=U[1:] - X1, k=k
                )

            elif risk_measure == "ADD":
                risk = 1 / n * cv.sum(U[1:] - X1)
                constraints += self._max_risk_measure_constraint(obj, risk, k)
            elif risk_measure == "CDaR":
                CDaR = cv.Variable((1, 1))
                Zd = cv.Variable((nav.shape[0], 1))

                risk = CDaR + 1 / (self.conditional_alpha * n) * cv.sum(Zd)
                risk_constraints += [
                    Zd * 1000 >= U[1:] * 1000 - X1 * 1000 - CDaR * 1000,
                    Zd * 1000 >= 0,
                ]
                constraints += self._max_risk_measure_constraint(obj, risk, k)

            elif risk_measure == "UCI":
                risk = cv.norm(U[1:] * 1000 - X1 * 1000, "fro") / np.sqrt(n)
                risk_aversion = risk_aversion / 1000
                constraints += self._max_risk_measure_constraint(
                    obj, risk, k, scale=1 / 1000
                )

        constraints += self._asset_weight_and_number_constraints(obj, mu, w, k)

        constraints += self._linear_constraints(obj, w, k)
        constraints += self._min_assets_contraint(obj, w, k)

        c = np.array(self.benchmark_weights, ndmin=2)
        constraints += self._tracking_error_constraint(obj, returns, w, c, k, n)
        constraints += self._turnover_constraint(obj, w, c, k)
        constraints += self._min_return_constraint(obj, ret, k)

        constraints = constraints + risk_constraints

        # Optimization Process
        # defining objective function
        objective = None
        if obj == "Sharpe":
            if model == "Classic":
                if kelly == "exact":
                    constraints += [risk <= 1]
                    constraints += [
                        cv.constraints.ExpCone(gr, np.ones((n, 1)) @ k, k + returns @ w)
                    ]
                    objective = cv.Maximize(ret * 1000)
                elif kelly == "approx":
                    constraints += [risk <= 1]
                    # if risk_measure != "MV":
                    # constraints += devconstraints  # add constraints from 'MV'
                    objective = cv.Maximize(ret)
                else:
                    constraints += [mu @ w - rf0 * k == 1]
                    objective = cv.Minimize(risk * 1000)
            else:
                constraints += [mu @ w - rf0 * k == 1]
                objective = cv.Minimize(risk * 1000)
        elif obj == "MinRisk":
            objective = cv.Minimize(risk * 1000)
        elif obj == "Utility":
            objective = cv.Maximize(ret - risk_aversion * risk)
        elif obj == "MaxRet":
            objective = cv.Maximize(ret * 1000)

        try:
            prob = cv.Problem(objective, constraints)
            for solver in self.solvers:
                try:
                    prob.solve(solver=solver, **self.solver_params.get(solver, {}))
                except:
                    pass

                if w.value is not None:
                    break

            if obj == "Sharpe":
                weights = np.array(w.value / k.value, ndmin=2).T
            else:
                weights = np.array(w.value, ndmin=2).T

            if self.allow_shorts is False:
                weights = np.abs(weights) / np.sum(np.abs(weights)) * self.budget
        except:
            raise ValueError("No solution!")

        return pd.Series(
            {asset: weights[0, self.assets.index(asset)] for asset in self.assets}
        )

    def efficient_frontier(
            self,
            model="Classic",
            risk_measure="MV",
            kelly=False,
            rf=0,
            points=20,
            historical=True,
    ):
        """
        Calculates n portfolios in efficient frontier of selected
        risk measure, available with current assets and constraints.

        Parameters
        ----------
        model : str, default 'Classic'
            The model used for optimizing the portfolio.

            - 'Classic': uses estimates of expected return vector and covariance matrix based on historical data.
            - 'BL': '' based on Black Litterman model.
            - 'FM': '' based on risk model specified by user.
            - 'BLFM': '' based on Black Litterman applied to risk model specified by user.

        risk_measure : str, default 'MV'
            The risk measure used to optimize the portfolio.

            - 'MV': Standard Deviation.
            - 'MAD': Mean Absolute Deviation.
            - 'MSV': Semi Standard Deviation.
            - 'FLPM': First Lower Partial Moment (Omega Ratio).
            - 'SLPM': Second Lower Partial Moment (Sortino Ratio).

        kelly : str, default False
            Method used to calculate mean return.
            Only relevant for model == 'Classic'.

            - False : arithmetic return
            - "exact" :  logarithmic return.
            - "approx" : approximate logarithmic using first and second moment

        rf : float, default 0
            Risk free rate, must be in same period of assets returns.

        points : scalar, default 50
            Number of portfolios calculated from the efficient frontier.

        historical : bool, default True
            Which kind of returns used to calculate risk measures.
            Ignored if model == 'Classic'.

        Returns
        -------
        frontier : pd.DataFrame
            Weights of the portfolios.

        Notes
        -----
        If != 'MV' and assets > 100 & != MV recommended to use .frontier_limits to know
        range of expected return/risk.
        """

        mu, sigma, returns, nav = self._model_inputs(model, historical)

        alpha = self.conditional_alpha
        w_min, w_max = self.frontier_limits(
            model=model,
            risk_measure=risk_measure,
            kelly=kelly,
            rf=rf,
            historical=historical,
        )

        w_min = np.array(w_min, ndmin=2).T
        w_max = np.array(w_max, ndmin=2).T

        ret_min = (mu @ w_min).item()
        ret_max = (mu @ w_max).item()

        if risk_measure == "MV":
            risk_min = np.sqrt(w_min.T @ sigma @ w_min).item()
            risk_max = np.sqrt(w_max.T @ sigma @ w_max).item()
        elif risk_measure == "MAD":
            risk_min = mean_absolute_deviation(returns @ w_min)
            risk_max = mean_absolute_deviation(returns @ w_max)
        elif risk_measure == "MSV":
            risk_min = semi_deviation(returns @ w_min)
            risk_max = semi_deviation(returns @ w_max)
        elif risk_measure == "CVaR":
            risk_min = conditional_value_at_risk(returns @ w_min, alpha)
            risk_max = conditional_value_at_risk(returns @ w_max, alpha)
        elif risk_measure == "WR":
            risk_min = worst_realization(returns @ w_min)
            risk_max = worst_realization(returns @ w_max)
        elif risk_measure == "FLPM":
            risk_min = lower_partial_moment(returns @ w_min, rf, 1)
            risk_max = lower_partial_moment(returns @ w_max, rf, 1)
        elif risk_measure == "SLPM":
            risk_min = lower_partial_moment(returns @ w_min, rf, 2)
            risk_max = lower_partial_moment(returns @ w_max, rf, 2)
        else:
            raise NotImplementedError("risk_measure not implemented yet!")

        mus = np.linspace(ret_min, ret_max, points)
        risks = np.linspace(risk_min, risk_max, points)

        frontier = []
        current_max_risk_measure = self.max_risk_measure
        for i in range(len(risks)):
            if i == 0:
                w = self.optimization(
                    model=model,
                    risk_measure=risk_measure,
                    obj="MinRisk",
                    kelly=kelly,
                    rf=rf,
                    risk_aversion=0,
                    historical=historical,
                )
            else:
                self.max_risk_measure = risks[i]
                w = self.optimization(
                    model=model,
                    risk_measure=risk_measure,
                    obj="MaxRet",
                    kelly=kelly,
                    rf=rf,
                    risk_aversion=0,
                    historical=historical,
                )

            frontier.append(w)

        self.max_risk_measure = current_max_risk_measure  # reset to start value
        return pd.concat(frontier, axis=1)

    def frontier_limits(
            self, model="Classic", risk_measure="MV", kelly=False, rf=0, historical=True
    ):
        """
        Calculate 1) minimum risk, and 2) maximum return portfolios
        available with current assets and constraints.

        Parameters
        ----------
        model : str, default 'Classic'
            The model used for optimizing the portfolio.

            - 'Classic': uses estimates of expected return vector and covariance matrix based on historical data.
            - 'BL': '' based on Black Litterman model.
            - 'FM': '' based on risk model specified by user.
            - 'BLFM': '' based on Black Litterman applied to risk model specified by user.

        risk_measure : str, default 'MV'
            The risk measure used to optimize the portfolio.

            - 'MV': Standard Deviation.
            - 'MAD': Mean Absolute Deviation.
            - 'MSV': Semi Standard Deviation.
            - 'FLPM': First Lower Partial Moment (Omega Ratio).
            - 'SLPM': Second Lower Partial Moment (Sortino Ratio).
            - 'CVaR': Conditional Value at Risk.
            - 'WR': Worst Realization (Minimax)
            - 'MDD': Maximum Drawdown of uncompounded cumulative returns (Calmar Ratio).
            - 'ADD': Average Drawdown of uncompounded cumulative returns.
            - 'CDaR': Conditional Drawdown at Risk of uncompounded cumulative returns.
            - 'UCI': Ulcer Index of uncompounded cumulative returns.

        kelly : str, default False
            Method used to calculate mean return.
            Only relevant for model == 'Classic'.

            - False : arithmetic return
            - "exact" :  logarithmic return.
            - "approx" : approximate logarithmic using first and second moment

        rf : float, default 0
            Risk free rate, must be in same period of assets returns.

        historical : bool, default True
            Which kind of returns used to calculate risk measures.
            Ignored if model == 'Classic'.

        Returns
        -------
        limits : DataFrame
            Contains weights of the portfolios.

        Notes
        -----
        Recommended vs. self.efficient_frontier for large number of
        assets to understand range of expected return/risk.

        """

        w_min = self.optimization(
            model=model,
            risk_measure=risk_measure,
            obj="MinRisk",
            kelly=kelly,
            rf=rf,
            risk_aversion=0,
            historical=historical,
        )

        w_max = self.optimization(
            model=model,
            risk_measure=risk_measure,
            obj="MaxRet",
            kelly=kelly,
            rf=rf,
            risk_aversion=0,
            historical=historical,
        )

        if w_min is None or w_max is None:
            raise ValueError("Limits of the frontier not found!")
        else:
            return w_min, w_max

    def reset_inputs(self):
        """Reset all inputs parameters of src models."""

        attributes = [
            "mu",
            "cov",
            "mu_fm",
            "cov_fm",
            "mu_bl",
            "cov_bl",
            "mu_bl_fm",
            "cov_bl_fm",
            "returns_fm",
            "nav_fm",
            "cov_l",
            "cov_u",
            "cov_mu",
            "cov_sigma",
            "d_mu",
            "k_mu",
            "k_sigma",
        ]

        for attr in attributes:
            setattr(self, attr, None)

    def reset_all(self):
        """Reset portfolio object to default values."""

        self.allow_shorts = False
        self.short_max = 0.2
        self.long_max = 1
        self.budget = 1
        self.min_number_effective_assets = None
        self.max_assets = None
        self.factor_returns = None
        self.fem = None
        self.conditional_alpha = 0.05
        self.benchmark_detailed = True
        self.benchmark_returns = None
        self.benchmark_weights = None
        self.turnover_limit = None
        self.tracking_error_limit = None

        self.min_return = None
        self.max_risk_measure = None

        self.asset_inequality = None
        self.group_inequality = None
