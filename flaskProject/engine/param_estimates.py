import numpy as np
import pandas as pd
import sklearn.covariance as sklearn_cov
from numpy.linalg import inv


__all__ = [
    "mu_estimates",
    "cov_matrix",
    "black_litterman",
]

if __name__ == "__main__":
    pass


def mu_estimates(X, method="hist", smoothing_factor=0.94):
    """
    Calculate expected returns using selected method.

    Parameters
    ----------
    X : pd.DataFrame, shape [n periods, m securities]
        Returns matrix.

    method : str, can be {'hist', 'ewma1' or 'ewma2'}
        Method used to estimate expected returns.

        - 'hist': use historical estimates.
        - 'ewma1'': use ewma with adjust=True.
        - 'ewma2': use ewma with adjust=False.

    smoothing_factor : scalar, default 0.94.
        Smoothing factor of ewma methods.

    Returns
    -------
    mu : 1d-array
        The estimation of expected returns.

    Raises
    ------
    ValueError
        When the value cannot be calculated.

    """

    if not isinstance(X, pd.DataFrame):
        raise ValueError("X must be a pd.DataFrame!")

    assets = list(X.columns)

    if method == "hist":
        mu = np.array(X.mean(), ndmin=2)
    elif method == "ewma1":
        mu = np.array(X.ewm(alpha=1 - smoothing_factor).mean().iloc[-1, :], ndmin=2)
    elif method == "ewma2":
        mu = np.array(
            X.ewm(alpha=1 - smoothing_factor, adjust=False).mean().iloc[-1, :], ndmin=2
        )
    else:
        raise NotImplementedError(f"method {method} not implemented yet!")

    return pd.DataFrame(np.array(mu, ndmin=2), columns=assets)


def cov_matrix(X, method="hist", smoothing_factor=0.94, **kwargs):
    """
    Calculate the covariance matrix using the selected method.

    Parameters
    ----------
    X : pd.DataFrame, shape [n periods, m securities]
        Returns matrix.

    method : str, default 'hist'
        Method used to estimate covariance matrix:

        - 'hist': use historical estimates.
        - 'ewma1'': use ewma with adjust=True.
        - 'ewma2': use ewma with adjust=False.
        - 'ledoit': use Ledoit and Wolf Shrinkage method.
        - 'oas': use Oracle Approximation Shrinkage method.
        - 'shrunk': use basic Shrunk Covariance method.

    smoothing_factor : scalar, default 0.94.
        Smoothing factor of ewma methods.

    **kwargs:
        Other variables related to covariance estimation.

    Returns
    -------
    cov : nd-array
        Estimation of covariance matrix.

    Raises
    ------
    ValueError
        When the value cannot be calculated.

    """

    if not isinstance(X, pd.DataFrame):
        raise ValueError("X must be a DataFrame")

    assets = X.columns.tolist()

    if method == "hist":
        cov = np.cov(X.T)
    elif method == "ewma1":
        cov = X.ewm(alpha=1 - smoothing_factor).cov()
        item = cov.iloc[-1, :].name[0]
        cov = cov.loc[(item, slice(None)), :]
    elif method == "ewma2":
        cov = X.ewm(alpha=1 - smoothing_factor, adjust=False).cov()
        item = cov.iloc[-1, :].name[0]
        cov = cov.loc[(item, slice(None)), :]
    elif method == "ledoit":
        lw = sklearn_cov.LedoitWolf(**kwargs)
        lw.fit(X)
        cov = lw.covariance_
    elif method == "oas":
        oas = sklearn_cov.OAS(**kwargs)
        oas.fit(X)
        cov = oas.covariance_
    elif method == "shrunk":
        sc = sklearn_cov.ShrunkCovariance(**kwargs)
        sc.fit(X)
        cov = sc.covariance_
    else:
        raise NotImplementedError(f"method {method} not implemented yet!")

    cov = pd.DataFrame(np.array(cov, ndmin=2), columns=assets, index=assets)

    return cov


def black_litterman(
    X,
    w,
    views,
    views_mu,
    delta=1,
    rf=0,
    equilibrium=True,
    method_mu="hist",
    method_cov="hist",
    **kwargs,
):
    """
    Estimate expected returns and covariance matrix based on
    Black Litterman model.

    Parameters
    ----------
    X : pd.DataFrame, shape [n samples, m assets]
        Assets matrix.

    w : pd.DataFrame, shape [m assets, 1]
        Weights vector.

    views : pd.DataFrame, shape [p views, m assets]
        Analyst's views matrix, can be relative or absolute.

    views_mu : pd.DataFrame, shape [p views, 1]
        Expected returns of analyst's views.

    delta : float, default 1
        Risk aversion factor.

    rf : scalar, default 0
        Risk free rate.

    equilibrium : bool, default True
        Indicate whether to use equilibrum or historical excess returns.

    method_mu
        See mu_estimates.

    method_cov
        See cov_matrix.

    **kwargs : dict
        Other variables related to expected returns and covariance estimation.

    Returns
    -------
    mu : DataFrame
        Mean vector.
    cov : DataFrame
        Covariance matrix.
    w : DataFrame
        Equilibrium weights, without constraints.

    Raises
    ------
    ValueError
        When the value cannot be calculated.

    """

    assets = list(X.columns)

    w = np.array(w, ndmin=2)
    if w.shape[0] == 1:
        w = w.T

    mu = np.array(mu_estimates(X, method=method_mu, **kwargs), ndmin=2)
    sigma = np.array(cov_matrix(X, method=method_cov, **kwargs), ndmin=2)

    views = np.array(views, ndmin=2)
    views_mu = np.array(views_mu, ndmin=2)

    tau = 1 / X.shape[0]
    Omega = np.array(np.diag(np.diag(views @ (tau * sigma) @ views.T)), ndmin=2)

    if equilibrium:
        PI = delta * (sigma @ w)
    else:
        PI = mu.T - rf

    PI_ = inv(inv(tau * sigma) + views.T @ inv(Omega) @ views) @ (
        inv(tau * sigma) @ PI + views.T @ inv(Omega) @ views_mu
    )
    M = inv(inv(tau * sigma) + views.T @ inv(Omega) @ views)

    mu = (PI_ + rf).T
    cov = sigma + M
    w = inv(delta * cov) @ PI_

    mu = pd.DataFrame(mu, columns=assets)
    cov = pd.DataFrame(cov, index=assets, columns=assets)
    w = pd.DataFrame(w, index=assets)

    return mu, cov, w
