import numpy as np
import pandas as pd
from scipy import linalg as LA
from statsmodels.stats.correlation_tools import cov_nearest

def generate_returns(asset_pricing: pd.DataFrame, tickers: list) -> pd.DataFrame:
    """Generate returns from pricing data.

    Args:
        data: pd.DataFrame
        securities: list
    """
    reduced_pricing = asset_pricing[asset_pricing['ticker'].isin(tickers)].sort_values(['ticker', 'date'])
    reduced_pricing['date'] = pd.to_datetime(reduced_pricing['date'], errors='coerce')
    reduced_pricing['price_change'] = reduced_pricing['adj_close'].pct_change()
    avg_reduced_pricing = reduced_pricing[['ticker', 'date', 'price_change']].groupby(
        ['ticker', 'date']).mean().reset_index()
    returns = pd.pivot(avg_reduced_pricing, index='date', columns='ticker', values='price_change').dropna()
    return returns

def is_positive_definite(cov, threshold=1e-8):
    """
    Indicate if a matrix is positive (semi)definite.

    Parameters
    ----------
    cov : nd-array, shape [n factors, n factors]
        Covariance matrix.

    threshold : scalar, default 1e-8

    Returns
    -------
    value : bool
        True if matrix is positive (semi)definite.

    Raises
    ------
        ValueError when the value cannot be calculated.

    """
    cov_ = np.array(cov, ndmin=2)
    w, V = LA.eigh(cov_, lower=True, check_finite=True)
    value = np.all(w >= threshold)

    return value


def corr_matrix(cov):
    """
    Generate a correlation matrix from a covariance matrix cov.

    Parameters
    ----------
    cov : nd-array, shape [n factors, n factors]
        Covariance matrix.

    Returns
    -------
    corr : nd-array
        A correlation matrix.

    Raises
    ------
        ValueError when the value cannot be calculated.

    """
    is_dataframe = isinstance(cov, pd.DataFrame)
    columns = None

    if is_dataframe:
        columns = cov.columns.tolist()

    cov1 = np.array(cov, ndmin=2)
    corr = np.array(cov, ndmin=2)
    m, n = cov.shape
    for i in range(0, m):
        for j in range(0, n):
            corr[i, j] = cov1[i, j] / np.sqrt(cov1[i, i] * cov1[j, j])

    if is_dataframe:
        corr = pd.DataFrame(corr, index=columns, columns=columns)

    return corr


def cov_fix(cov, method="clipped", **kwargs):
    """
    Fix a covariance matrix to a positive definite matrix.

    Parameters
    ----------
    cov : nd-array, shape [n factors, n factors]
        Covariance matrix.

    method : str, default 'clipped'.
        See statsmodels.stats.correlation_tools.cov_nearest.

    Returns
    -------
    cov_ : bool
        A positive definite covariance matrix.

    Raises
    ------
        ValueError when the value cannot be calculated.

    """
    is_dataframe = isinstance(cov, pd.DataFrame)
    columns = None

    if is_dataframe:
        columns = cov.columns.tolist()

    cov_ = np.array(cov, ndmin=2)
    cov_ = cov_nearest(cov_, method=method, **kwargs)
    cov_ = np.array(cov_, ndmin=2)

    if is_dataframe:
        cov_ = pd.DataFrame(cov_, index=columns, columns=columns)

    return cov_
