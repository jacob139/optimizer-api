import numpy as np
import pandas as pd
from dataclasses import dataclass

if __name__ == "__main__":
    pass


@dataclass
class Constraint:
    type: str
    sign: str
    disabled: bool = False
    set: str = None
    position: str = None
    weight: float = None
    type_relative: str = None
    relative_set: str = None
    relative: str = None
    factor: float = None


def _sign(c):
    if c.sign == ">=":
        value = 1
    elif c.sign == "<=":
        value = -1
    else:
        raise NotImplementedError("sign?")
    return value


def assets_constraints(constraints, asset_classes):
    """
    Create the linear constraints for assets (A) and groups of assets (B).

    Parameters
    ----------
    constraints : list of Constraint
        List of constraints with relevant parameters.
        See Constraint.

        Field values include:

        - type : {'Assets', 'Classes', 'All Assets', 'Each asset in a class' and 'All Classes'}
        - sign : {'>=', '<='}
        - disabled : if constraint is valid
        - set : group label
        - position : name of the asset or group.
        - weight : maximum or minimum weight of the absolute constraint.
        - type_relative : {'Assets', 'Classes'}
        - relative_set : group label for type_relative if 'Classes'
        - relative : name of the asset or asset class of the relative constraint.
        - factor : factor of the relative constraint.

    asset_classes : pd.DataFrame, shape [m assets, l cols]
        First column is asset list. Subsequent columns are different
        class sets for each asset.

    Returns
    -------
    A : nd-array
        Asset level constraints.

    B : nd-array
        Group level constraints.

    Raises
    ------
        ValueError when the value cannot be calculated.

    Examples
    --------
        asset_classes = {'Assets': ['FB', 'GOOGL', 'NTFX', 'BAC', 'WFC', 'TLT', 'SHV'],
                         'Class 1': ['Equity', 'Equity', 'Equity', 'Equity', 'Equity', 'Fixed Income', 'Fixed Income'],
                         'Class 2': ['Info Tech', 'Info Tech', 'Info Tech', 'Financials', 'Financials', 'Treasury', 'Treasury'],
                         }

    Notes
    -----
    Probably a more flexible way to do this!

    """

    constraints = pd.DataFrame(constraints)
    asset_classes = pd.Dataframe(asset_classes)

    assets = asset_classes.iloc[:, 0].values.tolist()
    number_of_assets = len(assets)

    A = []
    B = []
    for i, constraint in enumerate(constraints):
        if constraint.disabled:
            continue

        d = _sign(constraint)

        if constraint.type == "Assets":
            asset_index = assets.index(constraint.position)

            A1 = [0] * number_of_assets
            if constraint.weight is not None:
                A1[asset_index] = d
                A += A1
                B += [constraint.weight * d]
            else:
                A1[asset_index] = 1
                A2 = None
                if constraint.type_relative == "Assets":
                    relative_asset_index = assets.index(constraint.relative)
                    A2 = [0] * number_of_assets
                    A2[relative_asset_index] = 1
                elif constraint.type_relative == "Classes":
                    A2 = np.where(
                        asset_classes[constraint.relative_set].values
                        == constraint.relative,
                        1,
                        0,
                    )

                A1 = (
                    (np.array(A1) + np.array(A2) * constraint.factor * -1) * d
                ).tolist()
                A += A1
                B += [0]
        elif constraint.type == "All Assets":

            if constraint.weight is not None:
                A1 = (np.identity(number_of_assets) * d).tolist()
                B1 = np.ones((number_of_assets, 1)) * d * constraint.weight

                for l in range(number_of_assets):
                    A += A1[l]
                    B += B1.tolist()[0]
            else:
                A1 = np.identity(number_of_assets)
                A2 = None
                if constraint.type_relative == "Assets":
                    relative_asset_index = assets.index(constraint.relative)
                    A2 = np.zeros((number_of_assets, number_of_assets - 1))
                    A2 = np.insert(A2, relative_asset_index - 1, 1, axis=1)
                elif constraint.type_relative == "Classes":
                    A1 = np.identity(number_of_assets)
                    A2 = np.where(
                        asset_classes[constraint.relative_set].values
                        == constraint.relative,
                        1,
                        0,
                    )
                    A2 = np.ones((number_of_assets, number_of_assets)) * np.array(A2)

                A1 = (
                    (np.array(A1) + np.array(A2) * constraint.factor * -1) * d
                ).tolist()

                for l in range(number_of_assets):
                    A.append += A1
                    B += [0]
        elif constraint.type == "Classes":
            A1 = np.where(
                asset_classes[constraint.set].values == constraint.position, 1, 0
            )
            A2 = None

            if constraint.weight is not None:
                A1 = (np.array(A1) * d).tolist()
                A += A1
                B += [constraint.weight * d]
            else:
                if constraint.type_relative == "Assets":
                    relative_asset_index = assets.index(constraint.relative)
                    A2 = [0] * number_of_assets
                    A2[relative_asset_index] = 1
                elif constraint.type_relative == "Classes":
                    A2 = np.where(
                        asset_classes[constraint.relative_set].values
                        == constraint.relative,
                        1,
                        0,
                    )

                A1 = (
                    (np.array(A1) + np.array(A2) * constraint.factor * -1) * d
                ).tolist()
                A += A1
                B += [0]
        elif constraint.type == "Each asset in a class":
            A1 = np.where(
                asset_classes[constraint.set].values == constraint.position, 1, 0
            )
            A2 = None

            if constraint.weight is not None:
                for l, k in enumerate(A1):
                    if k == 1:
                        A3 = [0] * number_of_assets
                        A3[l] = 1 * d
                        A += A3
                        B += [constraint.weight * d]
            else:
                for l, k in enumerate(A1):
                    if k == 1:
                        A3 = [0] * number_of_assets
                        A3[l] = 1
                        if constraint.type_relative == "Assets":
                            relative_asset_index = assets.index(constraint.relative)
                            A2 = [0] * number_of_assets
                            A2[relative_asset_index] = 1
                        elif constraint.type_relative == "Classes":
                            A2 = np.where(
                                asset_classes[constraint.relative_set].values
                                == constraint.relative,
                                1,
                                0,
                            )

                        A3 = (
                            (np.array(A3) + np.array(A2) * constraint.factor * -1) * d
                        ).tolist()

                        A += A3
                        B += [0]
        elif constraint.type == "All Classes":
            if constraint.weight is not None:
                for class_ in set(asset_classes[constraint.set]):
                    A1 = (
                        np.where(asset_classes[constraint.set].values == class_, 1, 0)
                        * d
                    )

                    A += A1.tolist()
                    B += [constraint.weight * d]
            else:
                A2 = None
                for class_ in set(asset_classes[constraint.set]):
                    A1 = np.where(asset_classes[constraint.set].values == class_, 1, 0)

                    if constraint.type_relative == "Assets":
                        relative_asset_index = assets.index(constraint.relative)
                        A2 = [0] * number_of_assets
                        A2[relative_asset_index] = 1

                    elif constraint.type_relative == "Classes":
                        A2 = np.where(
                            asset_classes[constraint.relative_set].values
                            == constraint.relative,
                            1,
                            0,
                        )

                    A3 = (
                        (np.array(A1) + np.array(A2) * constraint.factor * -1) * d
                    ).tolist()
                    A += A3
                    B += [0]
        else:
            raise NotImplementedError("constraint type not recognized")

    A = np.array(A, ndmin=2)
    B = np.array(B, ndmin=2)

    return A, B


def factors_constraints(constraints, loadings):
    """
    Create the linear constraints for factors, C & D.

    Parameters
    ----------
    constraints : list of Constraint
        List of constraints with relevant parameters.
        See Constraint.

        Field values include:

        - sign : {'>=', '<='}
        - disabled : if constraint is valid
        - factor : name of the factor of the constraint
        - value : maximum or minimum value of the factor.
        - relative : name of factor for relative constraint

    loadings : DataFrame of shape (n_assets, n_features)
        The loadings matrix.

    Returns
    -------
    C : nd-array

    D : nd-array

    Raises
    ------
        ValueError when the value cannot be calculated.

    Examples
    --------
    ::

        loadings = {'const': [0.0004, 0.0002, 0.0000, 0.0006, 0.0001, 0.0003, -0.0003],
                    'momentum': [0.1916, 1.0061, 0.8695, 1.9996, 0.0000, 0.0000, 0.0000],
                    'quality': [0.0000, 2.0129, 1.4301, 0.0000, 0.0000, 0.0000, 0.0000],
                    'size': [0.0000, 0.0000, 0.0000, 0.4717, 0.0000, -0.1857, 0.0000],
                    'min_vol': [-0.7838, -1.6439, -1.0176, -1.4407, 0.0055, 0.5781, 0.0000],
                    'value': [1.4772, -0.7590, -0.4090, 0.0000, -0.0054, -0.4844, 0.9435]}

        loadings = pd.DataFrame(loadings)

        constraints = {'disabled': [False, False, False],
                       'factor': ['momentum', 'min_vol', 'value'],
                       'sign': ['<=', '<=', '>='],
                       'value': [0.9, -1.2, 0.3],
                       'relative': ['min_vol', '', '']}

    """

    C = []
    D = []
    for i, constraint in enumerate(constraints):
        if constraint.disabled:
            continue

        d = _sign(constraint)

        C1 = loadings[constraint.factor].values
        if constraint.relative is not None:
            C2 = loadings[constraint.relative].values
            C1 = C1 - C2
        C += C1 * d
        D += [constraint.value * d]

    C = np.array(C, ndmin=2)
    D = np.array(D, ndmin=2)

    return C, D
