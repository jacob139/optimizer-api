import numpy as np
from functools import wraps


__all__ = [
    "mean_absolute_deviation",
    "semi_deviation",
    "value_at_risk",
    "sharpe_risk",
    "sharpe_ratio",
]

if __name__ == "__main__":
    pass


def vector(func):
    @wraps(func)
    def wrapper(x, **kwargs):
        s0, s1 = x.shape

        if s0 != 1 and s1 != 1:
            raise ValueError("series must be vector!")
        elif x.shape[0] < x.shape[1]:
            x = x.T

        return func(np.array(x, ndmin=2), **kwargs)

    return wrapper


@vector
def mean_absolute_deviation(X):
    """Calculate Mean Absolute Deviation (MAD) of a returns series."""

    value = np.mean(np.absolute(X - np.mean(X, axis=0)), axis=0)
    value = np.array(value).item()

    return value


@vector
def semi_deviation(X):
    """Calculate the Semi Deviation of a returns series."""

    mu = np.mean(X, axis=0)
    value = mu - X
    n = value.shape[0]
    value = np.sum(np.power(value[np.where(value >= 0)], 2)) / (n - 1)
    value = np.power(value, 0.5).item()

    return value


@vector
def value_at_risk(X, alpha=0.05):
    """Calculate the Value at Risk (VaR) of a returns series."""

    sorted_a = np.sort(X, axis=0)
    index = int(np.ceil(alpha * len(sorted_a)) - 1)
    value = -sorted_a[index]
    value = np.array(value).item()

    return value


@vector
def conditional_value_at_risk(X, alpha=0.05):
    """Calculate the Conditional Value at Risk (CVaR) of a returns series."""

    X = np.sort(X, axis=0)
    index = int(np.ceil(alpha * len(X)) - 1)
    sum_var = 0
    for i in range(0, index + 1):
        sum_var = sum_var + X[i] - X[index]

    value = -X[index] - sum_var / (alpha * len(X))
    value = np.array(value).item()

    return value


@vector
def worst_realization(X):
    """Calculate the Worst Realization (WR) or Worst Scenario of a returns series."""

    X = np.sort(X, axis=0)
    value = np.array(-X[0]).item()

    return value


@vector
def lower_partial_moment(X, min_return=0, p=1):
    """
    Calculate the First (Omega) or Second (Sortino)
    Lower Partial Moment of a returns series.

    Parameters
    ----------
    X : 1d-array
        Returns series.
    min_return : float, 0
        Minimum acceptable return.
    p : float, can be {1,2}

    Returns
    -------
    value : float
        p-th Lower Partial Moment of a returns series.

    Raises
    ------
    ValueError
        When the value cannot be calculated.

    """

    if p not in [1, 2]:
        raise NotImplementedError("p can only be 1 or 2!")

    value = min_return - X

    if p == 2:
        n = value.shape[0] - 1
    else:
        n = value.shape[0]

    value = np.sum(np.power(value[np.where(value >= 0)], p)) / n
    value = np.power(value, 1 / p).item()

    return value


@vector
def sharpe_risk(w, cov=None, returns=None, risk_measure="MV", rf=0, alpha=0.05):
    """
    Calculate risk measure available on the Sharpe function.

    Parameters
    ----------
    w : vector-like, shape [m assets, 1]
        Weights matrix.

    cov : DataFrame or nd-array of shape (n_features, n_features)
        Covariance matrix.

    returns : pd.DataFrame, shape [periods, m assets]
        Historical returns of asset candidates.

    risk_measure : str, default 'MV'
        The risk measure used to optimize the portfolio.

        - 'MV': Standard Deviation.
        - 'MAD': Mean Absolute Deviation.
        - 'MSV': Semi Standard Deviation.
        - 'FLPM': First Lower Partial Moment (Omega Ratio).
        - 'SLPM': Second Lower Partial Moment (Sortino Ratio).
        - 'VaR': Value at Risk.
        - 'CVaR': Conditional Value at Risk.
        - 'WR': Worst Realization (Minimax)

    rf : float, default 0
        Risk free rate, must be in same period of assets returns.

    alpha : float, default 0.05
        Significance level of VaR, CVaR and CDaR.

    Raises
    ------
    ValueError
        When the value cannot be calculated.

    Returns
    -------
    value : float
        Risk measure of portfolio.

    """

    cov = np.array(cov, ndmin=2)
    returns = np.array(returns, ndmin=2)
    returns_ = returns @ w

    if risk_measure == "MV":
        risk = w.T @ cov @ w
        risk = np.sqrt(risk.item())
    elif risk_measure == "MAD":
        risk = mean_absolute_deviation(returns_)
    elif risk_measure == "MSV":
        risk = semi_deviation(returns_)
    elif risk_measure == "FLPM":
        risk = lower_partial_moment(returns_, min_return=rf, p=1)
    elif risk_measure == "SLPM":
        risk = lower_partial_moment(returns_, min_return=rf, p=2)
    elif risk_measure == "VaR":
        risk = value_at_risk(returns_, alpha=alpha)
    elif risk_measure == "CVaR":
        risk = conditional_value_at_risk(returns_, alpha=alpha)
    elif risk_measure == "WR":
        risk = worst_realization(returns_)
    else:
        raise NotImplementedError("risk_measure not implemented yet!")

    return risk


@vector
def sharpe_ratio(
    w, mu=None, cov=None, returns=None, risk_measure="MV", rf=0, alpha=0.05
):
    """
    Calculate risk-adjusted ratio from a portfolio returns series.

    Parameters
    ----------
    w : vector-like, shape [m assets, 1]
        Weights matrix.

    mu : DataFrame
        Mean vector.

    cov : DataFrame or nd-array of shape (n_features, n_features)
        Covariance matrix.

    returns : pd.DataFrame, shape [periods, m assets]
        Historical returns of asset candidates.

    risk_measure : str, default 'MV'
        The risk measure used to optimize the portfolio.

        - 'MV': Standard Deviation.
        - 'MAD': Mean Absolute Deviation.
        - 'MSV': Semi Standard Deviation.
        - 'FLPM': First Lower Partial Moment (Omega Ratio).
        - 'SLPM': Second Lower Partial Moment (Sortino Ratio).
        - 'VaR': Value at Risk.
        - 'CVaR': Conditional Value at Risk.
        - 'WR': Worst Realization (Minimax)

    rf : float, default 0
        Risk free rate, must be in same period of assets returns.

    alpha : float, default 0.05
        Significance level of VaR, CVaR and CDaR.

    Raises
    ------
    ValueError
        When the value cannot be calculated.

    Returns
    -------
    value : float
        Risk adjusted return ratio of :math:`X`.

    """

    mu = np.array(mu, ndmin=2)
    cov = np.array(cov, ndmin=2)
    returns = np.array(returns, ndmin=2)

    ret = (mu @ w).item()
    risk = sharpe_risk(
        w, cov=cov, returns=returns, risk_measure=risk_measure, rf=rf, alpha=alpha
    )
    return (ret - rf) / risk
