import flask
from flask import Flask, request, jsonify, Response
from engine import portfolio, util

import pandas as pd

app = Flask(__name__)

pricing = pd.read_csv("s3://allio-ds/dev/01_raw/asset_prices.csv")

@app.route('/')
def home():
    return jsonify('Welcome to Allio Rest API')


@app.route('/optimizer', methods=['POST'])
def predict():
    tickers = request.get_json()
    monthly_returns = util.generate_returns(pricing, tickers)
    port = portfolio.Portfolio(returns=monthly_returns)
    optimal_weights = port.optimization(model='Classic',
                                        risk_measure='MV',
                                        obj='Sharpe',
                                        rf=0,
                                        risk_aversion=0,
                                        historical=True).reset_index()
    optimal_weights.columns = ['assets', 'weights']
    return Response(optimal_weights.to_json(orient="records"))


if __name__ == '__main__':
    app.run(host='0.0.0.0', port=5000, debug=True)