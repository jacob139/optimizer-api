import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import risk_functions as rfx

__all__ = [
    "plot_frontier",
]

if __name__ == "__main__":
    pass


def plot_frontier(
    w_frontier,
    mu,
    cov=None,
    returns=None,
    risk_measure="MV",
    kelly=False,
    rf=0,
    conditional_alpha=0.05,
    optimal=None,
    current=None,
    t_factor=252,
    annotate_assets=None,
    ax=None,
    **kwargs,
):

    """
    Plot efficient frontier.

    Parameters
    ----------
    w_frontier : pd.DataFrame
        Portfolio weights of some points in  efficient frontier.

    mu : pd.DataFrame, shape [1, m assets]
        Vector of expected returns.

    cov : pd.DataFrame, shape [n features, m features)
        Covariance matrix, where n_features is the number of features.

    returns : DataFrame, shape [periods, n securities]
        Period returns of the assets.

    risk_measure : str, default 'MV'
        The risk measure used to optimize the portfolio.

        - 'MV': Standard Deviation.
        - 'MAD': Mean Absolute Deviation.
        - 'MSV': Semi Standard Deviation.
        - 'FLPM': First Lower Partial Moment (Omega Ratio).
        - 'SLPM': Second Lower Partial Moment (Sortino Ratio).
        - 'CVaR': Conditional Value at Risk.
        - 'WR': Worst Realization (Minimax)

    kelly : str, default False
        Method used to calculate mean return.
        Only relevant for model == 'Classic'.

        - False : arithmetic return
        - "exact" :  logarithmic return.
        - "approx" : approximate logarithmic using first and second moment

    rf : float, default 0
        Risk free rate, must be in same period of assets returns.

    conditional_alpha : float, default 0.05
        Significance level of VaR, CVaR, and CDaR.

    optimal : pd.Series, default None
        Optimal portfolio to highlight.

    current : pd.Series, default None
        Current portfolio to highlight, specified by the user.

    t_factor : float, default 252
        Annualization factor.

    annotate_assets : list-like
        Pass asset names to draw individual assets relative to frontier.

    ax : matplotlib axis, optional
        If provided, plot on this axis. The default is None.

    Raises
    ------
    ValueError
        When the value cannot be calculated.

    Returns
    -------
    ax : matplotlib Axes

    """

    if returns.shape[1] != w_frontier.shape[0]:
        raise ValueError("frontier vs. returns not aligned")

    if ax is None:
        ax = plt.gca()
        fig = plt.gcf()

    mu_ = np.array(mu, ndmin=2)

    if kelly is False:
        ax.set_ylabel("Expected Return")
    else:
        ax.set_ylabel("Expected Logarithmic Return")

    ax.set_xlabel(f"Expected Risk")  # - {risk_measure}")
    ax.set_title("Efficient Frontier")

    def _scatter(frontier_):
        X = []
        Y = []
        Z = []

        frontier_ = pd.DataFrame(frontier_)

        for i in range(frontier_.shape[1]):
            weights = np.array(frontier_.iloc[:, i], ndmin=2).T
            risk = rfx.sharpe_risk(
                weights,
                cov=cov,
                returns=returns,
                risk_measure=risk_measure,
                rf=rf,
                alpha=conditional_alpha,
            )

            if kelly is False:
                ret = mu_ @ weights
            else:
                ret = 1 / returns.shape[0] * np.sum(np.log(1 + returns @ weights))

            ret = ret.item() * t_factor

            if risk_measure not in ["MDD", "ADD", "CDaR", "EDaR", "UCI"]:
                risk = risk * t_factor ** 0.5

            ratio = (ret - rf) / risk

            X.append(risk)
            Y.append(ret)
            Z.append(ratio)

        return X, Y, Z

    X_, Y_, Z_ = _scatter(w_frontier)
    ax1 = ax.scatter(X_, Y_, c=Z_, **kwargs)

    def _extras(w, Xs, Ys, ax, marker="*", annotation=None):
        if w is not None:
            x, y, _ = _scatter(w)
            ax.scatter(x, y, marker=marker, s=16 ** 2, c="r")
            ax.annotate(annotation, (x[0] * 1.05, y[0]))
            Xs += x
            Ys += y
        return Xs, Ys, ax

    X_, Y_, ax = _extras(optimal, Xs=X_, Ys=Y_, ax=ax, marker="*", annotation="Optimal")
    X_, Y_, ax = _extras(
        current, Xs=X_, Ys=Y_, ax=ax, marker="o", annotation="Your Portfolio"
    )

    if annotate_assets is not None:
        mv = [np.sqrt(cov.iloc[x, x] * t_factor) for x in range(len(annotate_assets))]
        mu_ = np.squeeze(mu.values * t_factor).tolist()
        ax.scatter(mv, mu_, s=10, c="r", marker="o")
        for i, txt in enumerate(annotate_assets):
            ax.annotate(txt, (mv[i] * 1.01, mu_[i]))

        X_ += mv
        Y_ += mu_

    xmin = np.min(X_) - np.abs(np.max(X_) - np.min(X_)) * 0.1
    xmax = np.max(X_) + np.abs(np.max(X_) - np.min(X_)) * 0.1
    ymin = np.min(Y_) - np.abs(np.max(Y_) - np.min(Y_)) * 0.1
    ymax = np.max(Y_) + np.abs(np.max(Y_) - np.min(Y_)) * 0.1
    ax.set_ylim(ymin, ymax)
    ax.set_xlim(xmin, xmax)

    ax.xaxis.set_major_locator(plt.AutoLocator())

    ticks_loc = ax.get_yticks().tolist()
    ax.set_yticks(ax.get_yticks().tolist())
    ax.set_yticklabels(["{:.0%}".format(x) for x in ticks_loc])
    ticks_loc = ax.get_xticks().tolist()
    ax.set_xticks(ax.get_xticks().tolist())
    ax.set_xticklabels(["{:.0%}".format(x) for x in ticks_loc])

    ax.tick_params(axis="y", direction="in")
    ax.tick_params(axis="x", direction="in")

    ax.grid(linestyle=":")

    colorbar = ax.figure.colorbar(ax1)
    colorbar.set_label("Risk-adjusted Ratio")

    fig = plt.gcf()
    fig.tight_layout()

    return ax
